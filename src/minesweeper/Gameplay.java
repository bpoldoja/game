package minesweeper;
import java.util.Random;
import java.util.Scanner;
import java.util.InputMismatchException;

class Board {

	private int size;
	private int nrOfMines; // how many mines there are
	private String[][] privateBoardRepr; //board with numbers&mines shown
	private String[][] publicBoardRepr; //board with #
	private Random random = new Random();
	public boolean mineHit = false;
	private boolean printPrivateBoard = false;
	private int remaining;

	// Constructor
	Board(int boardSize) {
		if (boardSize <= 1) { //board size has to be greater than 1
			throw new RuntimeException("Invalid board size!");
		}
		size = boardSize;
		nrOfMines = size; // the number of mines are equal to the size that the player picks
		remaining = size * size; //
		privateBoardRepr = new String[size][size];
		publicBoardRepr = new String[size][size];

		int currNrOfMines = 0; //current number of mines
		while (currNrOfMines != nrOfMines) {
			currNrOfMines = 0;
			for (int row = 0; row < size; row++){
				for (int col = 0; col < size; col++){
					publicBoardRepr[row][col] = "[#]";
					if (random.nextInt(size) == 0 && currNrOfMines != nrOfMines){
						privateBoardRepr[row][col] = "[*]";
						currNrOfMines++;
					} else {
						privateBoardRepr[row][col] = "[0]";
					}
				}
			}
		}
		mineCounting();
	}

	/**
	 * Set printing private board field.
	 * If this field is true, then printing instances from this class will print the private board. (revealed board)
	 *
	 * @param b value to be set for printPrivateBoard field
	 */
	public void setPrintPrivateBoard(boolean b){
		printPrivateBoard = b;
	}

	private void mineCounting() {
		//for every square
		for (int row = 0; row < size; row++) {
			for (int col = 0; col < size; col++) {
				if (privateBoardRepr[row][col].equals("[*]")) {
					incrementSurrounding(row, col);
				}
			}
		}
	}
	/* Builds the private board by counting  mines on the board
	 * Adds 1 to squares surrounding mines
	 */
	private void incrementSurrounding(int row, int col) {
		// for all squares
		for (int currRow = row - 1; currRow != row + 2; currRow++) {
			for (int currCol = col - 1; currCol != col + 2; currCol++) {
				try {
					String square = privateBoardRepr[currRow][currCol];
					String nrString = Character.toString(square.charAt(1));
					if (nrString.equals("*")) {
						// skip adding 1 to mine
						continue;
					}
					Integer nr = Integer.parseInt(nrString);
					privateBoardRepr[currRow][currCol] = "[" + (nr + 1) + "]";
				} catch(IndexOutOfBoundsException e) {
					//Do nothing if edge of board
				}
			}
		}
	}

	/**
	 * Takes user input and returns if field is valid
	 *
	 * @param input input for row&column
	 * @returns if "#" at input position
	 */
	public boolean isValid(int[] input) {
		int row = input[0];
		int col = input[1];

		//fits into board check
		if (row <= size && row >= 0 && col < size && col >= 0) {
			String val = publicBoardRepr[row][col];
			if (val.equals("[#]")) {
				return true;
			}
		}
		return false;
	}

	/** Sets the squares in private & public board
	 * If player finds a mine (*), sets mineHit to true. (Basically game is over)
	 * If
	 *
	 * @param input takes them
	 */
	public void takeSquare(int[] input) {
		int row = input[0];
		int col = input[1];
		remaining--; //keep track of remaining hidden fields
		publicBoardRepr[row][col] = privateBoardRepr[row][col];
		if (publicBoardRepr[row][col].equals("[*]")) {
			mineHit = true;
		}
	}

	public String toString() {
		String boardStr = "";
		String lastRow = "    ";

		//for board layout
		for (int row = 0; row < size; row++){
			boardStr += (row + 1) + "|";
			for (int col = 0; col < size; col++){
				if (printPrivateBoard) {
					boardStr += " " + privateBoardRepr[row][col];
				} else {
					boardStr += " " + publicBoardRepr[row][col];
				}
			}
			boardStr += "\n\n";
			lastRow += (row + 1) + "   ";
		}
		return boardStr + lastRow;
	}
	/* if there are as many untaken fields as mines (Basically game over, player wins)
	 */
	public boolean onlyMinesLeft() {
		return remaining == nrOfMines;
	}

}

class Game {

	Board board;
	Player player;
	String result;

	Game(Player player, Board board) {
		this.board = board;
		this.player = player;
	}

	/**
	 * checks if the player has hit the mine
	 * @returns true when the result is "You Lose"
	 */
	public boolean isOver() {

		if (board.mineHit){
			result = "You lost!";
			return true;
		} else if (board.onlyMinesLeft()) {
			result = "You win!";
			return true;
		} else {
			return false;
		}
	}

}

class Player {

	Scanner input = new Scanner(System.in);

	/*
	* Gets players input to set row and column
	* Handles user invalid input
	*/
	public int[] getInput() {
		System.out.println("Rida?: ");
		String row = input.nextLine();
		System.out.println("Veerg?: ");
		String col = input.nextLine();
		try {
			int[] input = {Integer.parseInt(row) - 1, Integer.parseInt(col) - 1};
			return input;
		} catch(NumberFormatException e) {
			System.out.println("Invalid input format!");
			return getInput();
		}
	}
}

public class Gameplay {
	/**
	 * @param args
	 * Game starts here
	 * Player choses the board size
	 */
	public static void main(String[] args) {
		System.out.println("Enter board size (> 1): ");
		Scanner sc = new Scanner(System.in);
		//Input validity is checked here.
		int boardSize = 0;
		try {
			boardSize = sc.nextInt();
		} catch(InputMismatchException e){
			System.out.println("Invalid board size!");
			main(new String[]{});
			return;
		}

		Player player = new Player();
		Board board = new Board(boardSize);
		board.setPrintPrivateBoard(false); // hides the board(with mines)
		Game game = new Game(player, board);

		//Runs the game while the game is not over
		while (!game.isOver()) {
			System.out.println(game.board);
			int[] input = game.player.getInput();

			if (game.board.isValid(input)) {
				game.board.takeSquare(input);
			} else {
				System.out.println("Invalid input!");
				continue;
			}
		}
		//prints out the game result and the revealed board
		System.out.println("Game over: " + game.result); //if the game is over, prints the result
		board.setPrintPrivateBoard(true);
		System.out.println(game.board); //shows the full board, with mines
	}
}
